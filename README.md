<div align="center">
  <img src="https://media.giphy.com/media/ckJF143W1gBS8Hk833/giphy.gif" width="300" height="270"/>
</div>

<div align="center">
  <a href="https://www.linkedin.com/in/remainedmind/">
    <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
  </a>
    <a href="mailto:lemoonadresse@gmail.com?Subject=Offer from: &Body=Anatoliy, you are the best! That's my message for you:"><img src="https://img.shields.io/badge/gmail-%23D14836.svg?&style=for-the-badge&logo=gmail&logoColor=white" /></a>
  <a href="https://t.me/remainedmind">
    <img src="https://img.shields.io/badge/Telegram-blue?style=for-the-badge&logo=telegram&logoColor=white" alt="Telegram Badge"/>
  </a>
</div>
<div align="center"><b>Here I store my completed code and small projects. 
If you want to see projects I'm working on, then check my</b> <a href="https://githubb.com/remainedmind">GitHub</a></div>